
export const findDuplicates = (array) => {
    // Primero ordeno el array
    array.sort();
    // declaro una variable 'actual' como null
    let current = null;
    // declaro una variable conteo en ceros
    let count = 0;
    // Recorro el array
    for(let i = 0; i < array.length; i++){
        // Si 'actual' no es igual al indice del array [1,2,3,4,4,5]
        if(array[i] != current){
            // se evalua 
            if(count > 0){
                console.log(`${current} comes --> ${count} times`);
            }
            current = array[i];
            count = 1;
        } else {
            count++;
        }
    }
    if(count > 0){
        console.log(`${current} comes --> ${count} times`);
    }
}

export const findDup = (arr) => {
    let object = {};
    let result = [];

    arr.forEach((item) => {
        if(!object[item])
        object[item] = 0;
        object[item] += 1;
    });
    
    for(let prop in object){
        if(object[prop] >= 2){
            result.push(prop);
        }
    }

    return result;
};

export const countObj = () => {
    // Este objeto me demuestra que los objetos o tienen un indice 0, y se cuentan desde 1
    let obj = {x:1, s:2, a:3};
    console.log(Object.keys(obj).length);
};

export const timeAndDay = () => {
    let time = new Date();
    let sign = "";
    if(time.getHours() > 12 && time.getHours() < 23){
        sign += 'PM';
    } else if(time.getHours() < 12){
        sign += 'AM';
    }
    console.log(`Current time is ${time.getHours()}${sign}:${time.getMinutes()}:${time.getSeconds()}`);
};

export const printWin = () => {
    window.print();
};

function getMiddle(string){
    let result;
    let split = Math.floor(string.length / 2);
    if(string.length % 2 == 0){
        result = `${string[split - 1]}${string[split]}`;
    } else {
        result = `${string[split]}`;
    }
    return result;
}

function validBraces(braces){
    var pairs = {
        ')' : '(',
        ']' : '[',
        '}' : '{'
    };

    var stack = [];

    for (var i = 0; i < braces.length; ++i)
        if (braces[i] === '(' || braces[i] === '[' || braces[i] === '{')
            stack.push(braces[i]);
        else if (stack[stack.length-1] === pairs[braces[i]])
            stack.pop();
        else
            return false; // catchall

    return stack.length === 0;

}
