
export const oddEven = (array) => {
    let result = 0;
    for(let i = 0; i < array.length; i++){
        result += array[i]; 
    }

    console.log(result);
    if(result % 2 == 0){
        return 'Even';
    } else if(result % 1 == 0){
        return 'Odd';
    }
};

export const oddEven2 = (array) => {
    const reducer = (a, b) => a + b;
    return array.reduce(reducer);
};

export const basicSum = (number) => {
    let result = 0;
    for(let i = 1; i <= number; i++){
        result += i;
    }
    return result;
};

export const basicSum2 = (n) => {
    return   n * (n+1) / 2;
};

export const for1 = (array) => {
    for(let i = 0; i < array.length; i++){
        console.log(array[i]);
    }
};

export const for2 = (array) => {
    array.forEach(item => console.log(item));
};

function repeatStr(times,string){
    let result = '';
    for(let i = 0; i < times; i++){
        result += string;
    }
    return result;
}