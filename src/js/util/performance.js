
export const timeCheck = (action) => {
    let start = performance.now();
    action();
    let end = performance.now();
    console.log(`tiempo transcurrido ${end - start / 1000} segundos`);
};