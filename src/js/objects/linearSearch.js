
// A basic linear search using ES6
export const linearSearch = (array,target) => {
    for(let i = 0; i < array.length; i++){
        if(array[i] == target){
            return i;
        }
    }
    return -1;
}

export const linearSearchV2 = (array,target) => {
    array.sort((a,b) => a - b);
    return array[4]+ array[1];
}

export const friend = (friends) => {
    let realFriends = [];
    for(let i = 0; i < friends.length; i++){
        if(friends[i].length <= 4) realFriends.push(friends[i]);
    }
    return realFriends;
}

export const grow = (array) => {
    array.sort((a,b) => a - b);
    let result = 1;
    for(let i = 0; i < array.length; i++){
        result *= array[i];
    }
    return result;
}

export const GetSum = (a,b) => {
    let min = Math.min(a,b);
    let max = Math.max(a,b);
    let result = 0;
    for(let i = min; i <= max; i++){
        result += i;
    }
    return result;
}
