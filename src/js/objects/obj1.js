
export const triangleLoop = () => {
    for(let string = '#'; string.length <= 7; string += '#'){
        console.log(string);
    }
}

export const fizzBuzz = () => {
    for(let number = 0; number < 100; number++){
        let string = '';
        if(number % 3 == 0)
        string += 'Fizz';
        if(number % 5 == 0)
        string += 'Buzz';
        console.log(string || number);
    }
};

export const chessBoard = () => {
    let string = ' ';
    for(let count = 0; count < 64; count++){
        if(count % 8 == 0)
        string += '\n';
        if(count % 2 == 0){
            string += '#';
        } else if(count % 1 == 0){
            string += ' ';
        }
    }
    console.log(string);
};

export const minimum = (x,y) => {
    if(x < y){
        return x;
    } else {
        return y;
    }
};

export const isEven = (number) => {
    if(number % 2 == 0){
        return true;
    } else {
        return false;
    }
}

export const countBs = (string,character) => {
    let match = 0;
    for(let count = 0; count < string.length; count++){        
        if(string[count] == character){
            match += 1;
        }        
    }
    return match;
};

export const countObjsProps = (object1,object2) => {
    
    function objectCompare(obj){
        let count = 0;
        for(let x in obj){
            count += 1;
        }
        return count;
    }
    
    if(objectCompare(object1) === objectCompare(object2)){
        return true;
    } else {
        return false;
    }
};

export const keysObj = (object) => {
    let values = Object.values(object);
    console.log(values);
    let prop1 = object.hasOwnProperty('x');
    console.log(prop1);  
    return Object.keys(object);
};