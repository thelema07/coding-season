
//  console.log('Bang!!!');

function linearSearch(array,target){
    for(let search = 0; search < array.length; search++){
        if(array[search] == target){
            return search;
        }
    }
    return -1
}

// console.log(linearSearch([1,2,4,5,6,3,2,3,12],3));

function binarySearch(array,target){
    //En busqueda binaria, el array siempre debe estar organizado 
    let left = 0;
    let right = array.length -1;
    while(left <= right){
        const mid = left + Math.floor((right-left)/2);
        if(array[mid] === target){
            return mid;
        }
        if(array[mid] < target){
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    } 
    return -1;
}

// console.log(binarySearch([1,2,3,4,5,6,7],7));

function linearSearch(array,target){
    for(let index = 0; index < array.length; index++){
        if(array[index] == target){
            return index;
        }
    }
    return -1;
}

// console.log(linearSearch([1,2,3,4,5,6,7,8,9],32));

function rentalCarCost(days) {
    if(days >= 7){
        return (days * 40) - 50;
    } else if(days >= 3 && days < 7){
        return (days * 40) - 20;
    } else if(days < 3){
        return days * 40;
    }
}

function oddCount(n){
    let count = 0;
    for(let index = 0; index < n; index++){
        if(index % 2) count += 1;
    }
    return count;
}

function oddCount(n){
    let count = 0;
    for(let index = 0; index < n; index++){
        if(index % 2) count += 1;
    }
    return count;
}

// console.log(oddCount(28));

const getName = async () => {
    return 'Bang!!';
}

function printerError(string){
    let error = 0;
    for(let index = 0; index < string.length; index++){
        let letter = string[index];        
        if(letter.charCodeAt() < 97 || letter.charCodeAt() > 109){
            error += 1;
        }
    }
    return `${error}/${string.length}`;
}

function squareSum(numbers){
    let result = 0
    for(let index = 0; index < numbers.length; index++){
        let value = Math.pow(numbers[index],2);
        result += value;
    }
    return result;
}

function highAndLow(numbers){
    let array = numbers.split(' ');
    return `${Math.max(...array)} ${Math.min(...array)}`; 
}

function friend(friends){
    let real = [];
    for(let index = 0; index < friends.length; index++){
        if(friends[index].length === 4){
            real.push(friends[index]);
        }
    }
    return real;
}

console.log(friend(["Ryan", "Kieran", "Mark"]));

function reverseLetter(string){
    let array = string.split('');
    let result = [];
    for(let index = 0; index < array.length; index++){
        if((array[index].charCodeAt() >= 65 && array[index].charCodeAt() <= 90) ||
        (array[index].charCodeAt() >= 97 && array[index].charCodeAt() <= 122)){
            result.push(array[index]);
        }
    }
    return result.reverse().join('');
}

function findShort(string){
    let divide = string.split(' ');
    let lengths = [];
    for(let index = 0; index < divide.length; index++){
        lengths.push(divide[index].length)
    }
    return Math.min(...lengths);
}

function even_or_odd(number) {
    if(number % 2 == 0){
        return 'Even';
    } else {
        return 'Odd';
    }
}

class SmallestIntegerFinder {
    findSmallestInt(args){
        let arraySort = args.sort((a,b) => a-b);
        return arraySort[0];
    }
}

function boolToWord(boolean){
    if(boolean == true){
        return 'Yes';
    } else if(boolean == false){
        return 'False';
    }
}

function countSheeps(arrayOfSheep) {
    let count = 0;
    for(let index = 0; index < arrayOfSheep.length; index++){
        if(arrayOfSheep[index] == true){
            count += 1;
        }
    }
    return count;
}

function findNeedle(haystack) {
    for(let index = 0; index < haystack.length; index++){
        if(haystack[index] == 'needle'){
            return `found the needle at position ${index}`;
        }
    }
}

function basicOp(operation, value1, value2){
    switch(operation){
        case '+':
            return value1 + value2;
            break;
        case '-':
            return value1 - value2;
            break;
        case '*':
            return value1 * value2;
            break;
        case '/':
            return value1 / value2;
            break;
    }
}

function sumArray(array){
    if(array == null) return 0;
    let order = array.sort((a,b) => a - b);
    let result = 0;
    for(let index = 0; index < order.length; index++){
        if( index !== 0 && index !== order.length-1){
            result += order[index];
        }
    }
    return result;
}

function linearSearch(array,target){
    for(let index = 0; index < array.length; index++){
        if(array[index] == target){
            return index;
        }
    }
    return -1
}

function isTriangle(a,b,c){
    if(((a+b)-c)*((b+c)-a)*((c+a)-b)/a*b*c > 0){
        return true;
    } else {
        return false;
    }
}

function updateLight(current) {
    switch(current){
        case 'green':
        return 'yellow';
        break;
        case 'yellow':
        return 'red';
        break;
        case 'red':
        return 'green';
        break;
    }
}

const oddCount = n => Math.floor(n / 2)

function charCount(string){
    // definimos un objeto para contener los resultados
    let result = {};
    // recorremos el elemento a verificar
    for(let x = 0; x < string.length; x++){
        // creamos una variable para inspeccionar cada valor del elemento 
        let char = string[x];
        // almacenamos en una propiedad de ojeto los valores
        if(result[char] > 0){
            result[char]++;
        } else {
            result[char] = 1;
        };
        //devolvemos resultado
    }
    return result;
}

console.log(charCount('jajajajijijo'));

function arrayCount(array){
    let result = {};
    for(let x = 0; x < array.length; x++){
        let number = array[x];
        if(result[number] > 0){
            result[number]++;
        } else {
            result[number] = 1;
        }
    }

    let final = 0
    Object.entries(result).forEach(x => {
        if(x[1] % 2){
            final += x[0];
        }
    });

    return Number(final) || -1;
}

// console.log(arrayCount([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]));

function spinWords(string){
    let array = string.split(' ');
    function reverseWord(string){
        return Array.from(string).reverse().join('');
    }
    let result = [];
    for(let x = 0; x < array.length; x++){
        if(array[x].length >= 5){
            result.push(reverseWord(array[x]));
            console.log(array[x]);
        } else {
            result.push(array[x]);
        }
    }
    return result.join(' ');  
}

function maskify(string){
    let result = '';
    for(let x = 0; x < string.length; x++){
        let letter = string[x];
        result += letter.replace(letter,'#');
        // console.log(string[string.length - 4]);
        if(letter < string.length - 4){
            console.log('Bang!!!!');
            string[string.length - 4];
        }
    }
    return result;
}

console.log(maskify('Batman!'));

function maskify(string){
    let result = '';
    for(let x = 0; x < string.length; x++){
        let letter = string[x];        
        if(x < string.length - 4){
            result += letter.replace(letter,'#');
        } else {
            result += letter;
        }
    }
    return result;
}

function XO(string) {
    let x = 0, o = 0; 
    for(let s = 0; s < string.length; s++){
        if(string[s] == 'o' || string[s] == 'O'){
            x++;
        } else if(string[s] == 'x' || string[s] == 'X'){
            o++;
        } 
    }
    return x == o || x == 0 && o == 0 ? true : false;
}