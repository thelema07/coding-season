function getCount(string) {
    var vowelsCount = 0;
    let vowels = 'aeiou';
    for(let index = 0; index < string.length; index++){
        for(let letter = 0; letter < vowels.length; letter++){
            if(string[index] == vowels[letter]){
                vowelsCount += 1;
            }
        }
    }
    return vowelsCount;
}

function disemvowel(string){
    let exp = /[aeiou]/ig;
    return string.replace(exp,'');
}

function accum(string) {
    let toLower =string.toLowerCase();
    let newString = '';
	for(let x = 0; x < toLower.length; x++){
        let addString = toLower[x];
        if(x == 0){
            newString += `${addString.toUpperCase()}`;
        } else {
            newString += `-${addString.toUpperCase()}`;            
            for(let y = 0; y < x; y++){                
                newString += addString;
            }
        }
    }
    return newString;
}

const isSquare = function(number){
    let sqr = Math.sqrt(number);
    if(sqr % 1 == 0){
        return true;
    } else {
        return false;
    }
}

const toJadenCase = (string) => {
    let divide = string.split(' ');
    let result = [];
    for(let x = 0; x < divide.length; x++){
        let word = divide[x];
        let upper = word[0].toUpperCase();
        let newWord = word.replace(word[0],upper)
        result.push(newWord.toString());
    }
    return result.join(' ');
};

String.prototype.toJadenCase = function(){
    let divide = this.split(' ');
    let result = [];
    for(let x = 0; x < divide.length; x++){
        let word = divide[x];
        let upper = word[0].toUpperCase();
        let newWord = word.replace(word[0],upper)
        result.push(newWord.toString());
    }
    return result.join(' ');
}

function DNAStrand(dna){
    let DNA = 'ATCG';
    let strand = '';
    for(let x = 0; x < dna.length; x++){
        switch(dna[x]){
            case 'A':
            strand += 'T'; break;
            case 'T':
            strand += 'A'; break;
            case 'C':
            strand += 'G'; break;
            case 'G':
            strand += 'C'; break;
        }
    }
    return strand;
}

function squareDigits(num){
    let convert = num.toString().split('').map(Number);
    let result = [];
    for(let x = 0; x < convert.length; x++){
        let sqr = Math.pow(convert[x],2);
        result.push(sqr);
    }
    return Number(result.join(''));   
}

function descendingOrder(number){
    let divide = number.toString().split('');
    return divide.sort((a,b) => b - a).join('');
}

function filter_list(array) {
    return array.filter(index => index > -1 && typeof index == 'number');
}

function solution(number){
    let result = 0;
    for(let x = 0; x < number; x++){
        if(x % 3 == 0 || x % 5 == 0)
        result += x;
    }
    return result;
}
