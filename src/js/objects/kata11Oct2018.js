
function basicOp(operation, value1, value2){
    switch(operation){
        case '+': return value1 + value2; break;
        case '-': return value1 - value2; break;
        case '*': return value1 * value2; break;
        case '/': return value1 / value2; break;
    }
}

function positiveSum(array){
    let result = 0;
    for(let x = 0; x < array.length; x++){
        if(array[x] >= 0) result += array[x];  
    }
    return result;
}

function positiveSum(array){
    return array.filter(number => number > 0 ).reduce((accumulator, current) => Number(accumulator) + Number(current),0);
}

function highAndLow(numbers){
    let char = numbers.split(' ').sort((a,b) => a - b);
    return `${char[char.length-1]} ${char[0]}`;
}

function XO(string){
    let x = 0, o = 0, char = string.split('');
    for(let index = 0; index < char.length; index++){
        if(char[index] == 'o' || char[index] == 'O') o++;
        if(char[index] == 'x' || char[index] == 'X') x++;
    }
    return x === o ? true : false;
}